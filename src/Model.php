<?php

/**
 * Model - LMFDS Foundation Model class.
 *
 * Version 1.0.5
 */

namespace Lmfriends\LmfdsFoundation;

use mysqli;

class Model
{
  protected $_env;
  protected $_tableName;
  protected $_selectItems;
  protected $_join;
  protected $_condition;
  protected $_orderBy;

  protected $fillable = [
    'mall_id',
    'shop_no',
    'client_id',
  ];

  public function __construct($env, $tableName)
  {
    $prefix = $_ENV['DB_TABLE_PREFIX'] ?? '';
    $this->_env = $env;
    $this->_tableName = $prefix . $tableName;
    $this->initSql();
  }

  public function getFillable($data)
  {
    $fillable = $this->fillable;
    return array_filter($data, function ($var, $key) use ($fillable) {
      return in_array($key, $fillable);
    }, ARRAY_FILTER_USE_BOTH);
  }

  protected function initSql()
  {
    $this->_selectItems = "*";
    $this->_join = "";
    $this->_condition = "";
    $this->_orderBy = "ORDER BY id DESC";
  }

  public function select($option)
  {
    $this->_selectItems = $option;
    return $this;
  }

  public function innerJoin($option)
  {
    if ($this->_join === '') $this->_join = "INNER JOIN $option";
    else $this->_join .= " INNER JOIN $option";
    return $this;
  }

  public function leftJoin($option)
  {
    if ($this->_join === '') $this->_join = "LEFT JOIN $option";
    else $this->_join .= " LEFT JOIN $option";
    return $this;
  }

  public function rightJoin($option)
  {
    if ($this->_join === '') $this->_join = "RIGHT JOIN $option";
    else $this->_join .= " RIGHT JOIN $option";
    return $this;
  }

  public function joinRaw($str)
  {
    if ($this->_join === '') $this->_join = $str;
    else $this->_join .= " $str";
    return $this;
  }

  public function where(...$params)
  {
    $str = $this->stringCondition($params);
    if ($str === false) return $this;

    if ($this->_condition === '') $this->_condition = "WHERE {$str}";
    else $this->andWhere(...$params);
    return $this;
  }

  public function whereIn($field, $value)
  {
    if (is_array($value)) $value = implode(',', $value);
    if ($this->_condition === '') $this->_condition = "WHERE $field IN({$value})";
    else $this->_condition .= " AND $field IN({$value})";
    return $this;
  }

  public function whereRaw($param)
  {
    if ($this->_condition === '') $this->_condition = "WHERE {$param}";
    else $this->_condition .= " AND {$param}";
    return $this;
  }

  public function orWhereRaw($param)
  {
    if ($this->_condition === '') $this->_condition = "WHERE {$param}";
    else $this->_condition .= " OR {$param}";
    return $this;
  }

  public function andWhere(...$params)
  {
    $str = $this->stringCondition($params);
    if ($str === false) return $this;

    $this->_condition .= " AND {$str}";
    return $this;
  }

  public function orWhere(...$params)
  {
    $str = $this->stringCondition($params);
    if ($str === false) return $this;

    $this->_condition .= " OR {$str}";
    return $this;
  }

  public function orderBy($option)
  {
    $this->_orderBy = "ORDER BY " . $option;
    return $this;
  }

  protected function toSql($option = null)
  {
    return "SELECT {$this->_selectItems}
      FROM {$this->_tableName}
      {$this->_join}
      {$this->_condition}
      {$this->_orderBy}";
  }

  public function findAll($option = null)
  {
    $sql = $this->toSql($option);
    $result = $this->queryExecute($sql);
    return $result;
  }

  public function count()
  {
    $condition = $this->_condition;
    $sql = "SELECT count(*) as cnt
      FROM {$this->_tableName}
      {$this->_join}
      $condition";
    $row = $this->queryExecute($sql);
    return isset($row[0]) ? intval($row[0]['cnt']) : 0;
  }

  public function findById($id)
  {
    $condition = "WHERE id = $id";
    $sql = "SELECT {$this->_selectItems} FROM {$this->_tableName} $condition";
    $row = $this->queryExecute($sql);
    return isset($row[0]) ? $row[0] : null;
  }

  public function updateById($id, $data)
  {
    $condition = "WHERE id = $id";

    $querySet = [];
    foreach ($data as $key => $value) {
      if (is_array($value)) $value = addslashes(json_encode($value, JSON_UNESCAPED_UNICODE));
      array_push($querySet, $value === null ? "$key = null" : "$key = '$value'");
    }
    $querySet = implode(', ', $querySet);

    $sql = "UPDATE {$this->_tableName} SET $querySet $condition";
    return $this->queryExecute($sql);
  }

  public function update($data)
  {
    $condition = $this->_condition;
    if (!$condition) return false;

    $querySet = [];
    foreach ($data as $key => $value) {
      if (is_array($value)) $value = addslashes(json_encode($value, JSON_UNESCAPED_UNICODE));
      array_push($querySet, $value === null ? "$key = null" : "$key = '$value'");
    }
    $querySet = implode(', ', $querySet);

    $sql = "UPDATE {$this->_tableName} SET $querySet $condition";
    return $this->queryExecute($sql);
  }

  public function deleteById($id)
  {
    $condition = "WHERE id = $id";
    $sql = "DELETE FROM {$this->_tableName} $condition";
    return $this->queryExecute($sql);
  }

  public function delete()
  {
    $condition = $this->_condition;
    if (!$condition) return false;

    $sql = "DELETE FROM {$this->_tableName} $condition";
    return $this->queryExecute($sql);
  }

  public function paginate($perPage = 15)
  {
    $page = intval($_GET['page'] ?? '1');
    $offset = ($page - 1) * $perPage;
    $condition = $this->_condition;
    $join = $this->_join;
    $sql = "SELECT {$this->_selectItems}
      FROM {$this->_tableName}
      {$this->_join}
      $condition
      {$this->_orderBy}
      LIMIT $perPage
      OFFSET $offset";
    $result = $this->queryExecute($sql);

    $pagination = [
      'total' => $condition
        ? $this->joinRaw($join)->whereRaw(str_replace('WHERE ', '', $condition))->count()
        : $this->count(),
      'per_page' => $perPage,
      'current_page' => $page,
      'data' => $result,
    ];
    return $pagination;
  }

  protected function queryExecute($sql)
  {
    // Create connection
    $conn = new mysqli($this->_env['host'], $this->_env['username'], $this->_env['password'], $this->_env['dbname']);
    $conn->set_charset($this->_env['charset'] ?: 'utf8');

    // Check connection
    if ($conn->connect_error) {
      return [
        'error' => [
          'message' => 'Connection failed: ' . $conn->connect_error,
          'host' => $this->_env['host'],
          'dbname' => $this->_env['dbname'],
          'username' => $this->_env['username'],
        ]
      ];
    }

    $retValue = null;
    $result = $conn->query($sql);
    if ($result === TRUE) {
      $retValue = ['success' => 'query successfully', 'insert_id' => $conn->insert_id, 'affected_rows' => $conn->affected_rows ?? 0];
    } else if (gettype($result) == 'object' && $result->num_rows >= 0) {
      $retValue = [];
      // output data of each row
      while ($row = $result->fetch_assoc()) {
        array_push($retValue, $row);
      }
    } else {
      $retValue = [
        'error' => [
          'sql' => $sql,
          'message' => $conn->error
        ]
      ];
    }

    $conn->close();
    $this->initSql();

    return $retValue;
  }

  protected function getValue($data, $key, $defaultValue = '')
  {
    if (isset($data[$key])) return $data[$key];
    return $defaultValue;
  }

  protected function stringCondition($params)
  {
    if (count($params) < 2) return false;

    $field = $params[0];
    $operator = count($params) === 2 ? '=' : $params[1];
    $value = count($params) === 2 ? $params[1] : $params[2];
    if (gettype($value) === "string") $value = $value === 'null' ? 'null' : "'$value'";

    return "{$field} {$operator} {$value}";
  }

  protected function get_caller_method()
  {
    $traces = debug_backtrace();
    return isset($traces[2]) ? $traces[2]['function'] : null;
  }

  protected function camel_to_snake($input)
  {
    return strtolower(preg_replace('/(?<!^)[A-Z]/', '_$0', $input));
  }

  protected function makeSqlQuery(...$params)
  {
    $caller = $this->get_caller_method();
    $arrCaller = explode('By', $caller);
    $arrItem = array_map(array($this, 'camel_to_snake'), explode('And', array_pop($arrCaller)));

    $arrCondtion = [];
    foreach ($arrItem as $idx => $item) {
      $value = gettype($params[$idx]) == 'string' ? "'{$params[$idx]}'" : $params[$idx];
      array_push($arrCondtion, "$item = $value");
    }

    if ($this->_condition === '') $condition = 'WHERE ' . join(' AND ', $arrCondtion);
    else $condition = $this->_condition . ' AND ' . join(' AND ', $arrCondtion);

    $command = strtoupper(array_pop($arrCaller));
    if ($command == 'FIND') $command = 'SELECT ' . $this->_selectItems;
    return $command == 'DELETE'
      ? $command . " FROM {$this->_tableName} $condition"
      : $command . " {$this->_selectItems} FROM {$this->_tableName} $condition";
  }

  protected function toStringValue($value)
  {
    $first = gettype($value) == 'string' ? substr($value, 0, 1) : '';
    $last = gettype($value) == 'string' && strlen($value) > 3 ? substr($value, -1) : '';
    if (gettype($value) == 'string') return $first == '@' && $last == '@' ? substr($value, 1, -1) : "'" . $value . "'";
    else if (gettype($value) == 'integer' || gettype($value) == 'double') return $value;
    else if (gettype($value) == 'array' || gettype($value) == 'object') return "'" . addslashes(json_encode($value, JSON_UNESCAPED_UNICODE)) . "'";
    else return $value ? $value : 'null';
  }

  protected function queryInsert($arr)
  {
    $arrProp = [];
    $arrValue = [];
    foreach ($arr as $prop => $value) {
      array_push($arrProp, $prop);
      array_push($arrValue, $value);
    }
    $strItems = join(',', $arrProp);
    $strValues = join(',', array_map(array($this, 'toStringValue'), $arrValue));
    return "INSERT INTO {$this->_tableName} ($strItems) VALUES ($strValues)";
  }
}
