<?php

/**
 * AuthorityRepository - API 권한 저장소
 *
 * Version 1.0.2
 */


namespace Lmfriends\LmfdsFoundation\Repositories;

use Lmfriends\LmfdsFoundation\Model;

class AuthorityRepository extends Model
{
  protected $fillable = [
    'platform',
    'mall_id',
    'code',
  ];

  public function __construct($env, $tableName = 'tb_authorities')
  {
    parent::__construct($env, $tableName);
  }

  public function findByMallId($platform, $mallId, $code, $strSelect = 'mall_id')
  {
    $sql = $this->select($strSelect)
      ->where('platform', $platform)
      ->where('code', $code)
      ->makeSqlQuery($mallId);
    $row = $this->queryExecute($sql);
    return isset($row[0]) ? $row[0] : false;
  }

  public function save($platform, $mallId, $code = null)
  {
    if (is_null($code)) $code = "$mallId@lmfds";
    if ($this->findByMallId($platform, $mallId, $code) !== false) return false;

    $insertData = [
      'platform' => $platform,
      'mall_id' => $mallId,
      'code' => $code,
    ];
    $sql = $this->queryInsert($insertData);
    return $this->queryExecute($sql);
  }
}
