# LMFDS Foundation class
- Model  
    : sql query rule (findByMallIdAndClientId)
- SQLRecord
- PDOHandler
- AuthorityRepository (tb_authorities)  
    : platform, mall_id


## 설치
composer.json에 다음과 같은 항목을 추가하고 composer update

```
"require": {
    "lmfriends/lmfds-foundation" : "~1.0.6"
},
"repositories": [
    {
        "type": "vcs",
        "url": "https://gitlab.com/php-packages1/lmfds-foundation"
    }
]
```
