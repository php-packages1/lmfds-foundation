CREATE TABLE `tb_authorities` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `platform` varchar(32) DEFAULT NULL COMMENT '서비스 플랫폼',
  `mall_id` varchar(64) NOT NULL COMMENT '쇼핑몰 아이디',
  `code` varchar(128) NOT NULL DEFAULT '',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
